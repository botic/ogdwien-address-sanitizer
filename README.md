# ogdwien-address-sanitizer

This repository contains unit tests to check and validate a sanitizers
which corrects and harmonizes the address data provided by the City of Vienna.

Examples:

* `Schottengasse VOR 6-8(STAENDE)` => `Schottengasse vor 6-8 (Stände)`
* `Stadtpark/C WC-ANLAGE` => `Stadtpark WC-Anlage`
* `Am Hof T Kunstmarkt` => `Am Hof Kunstmarkt`
* `Dr.-Ignaz-Seipel-Platz K Kirche` => `Dr.-Ignaz-Seipel-Platz Kirche`
* `Station Schwedenplatz STATION` => `Station Schwedenplatz`
* `Azaleengasse ECKE OLEANDERG.(LAGERPL.)` => `Azaleengasse Ecke Oleandergasse (Lagerplatz)`
* …
* 

## License

GPLv3 – so give back any modifications or additions ;-)